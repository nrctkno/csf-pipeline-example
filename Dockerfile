# Run with:
# $ docker build -t pipeline-example:latest .
# $ docker run -it --rm --mount src="$(pwd)/",target=/var/www/html/,type=bind pipeline-example:latest sh
# $ cd tools && php ../composer.phar install && cd ..

FROM php:7.2.34-zts-alpine

WORKDIR /var/www/html/

RUN curl -sS https://getcomposer.org/installer | php
