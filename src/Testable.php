<?php

interface Testable
{
    public function do();

    public function defineStuff();
}
