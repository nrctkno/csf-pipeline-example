<?php

$finder = PhpCsFixer\Finder::create()
    ->in('src')
    //->exclude(['vendor', 'src2'])
;


return (new PhpCsFixer\Config())
    ->setFinder($finder)
    ->setUsingCache(false)
    ->setRules(
        [
            '@PSR12' => true,
            'strict_param' => true,
            'array_syntax' => ['syntax' => 'short'],
        ]
    );
