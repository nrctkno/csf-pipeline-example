# CSF-Pipeline Example


Implementation of Symfony's Coding Standards Fixer in a Gitlab pipeline.


## Workflow

- In your repo settings (i.e. this: https://gitlab.com/nrctkno/csf-pipeline-example/edit ) go to general, expand _Merge requests_ and enable the option _Merge checks > pipelines must succeed_.
- Clone the code. Create a branch, change something.
- Push it and create a merge request. A detached pipeline should run. If the pipeline fails, the merge will be blocked.


## Recipe (how was the project built)

- Run `composer init`
- Run `composer require friendsofphp/php-cs-fixer --dev`
- Create some test code
- Create `.php-cs-fixer.dist.php`

See [usage](https://cs.symfony.com/doc/usage.html) , [built-in rules](https://cs.symfony.com/doc/rules/index.html) , [rule sets](https://cs.symfony.com/doc/ruleSets/index.html) and [config](https://cs.symfony.com/doc/config.html) for detailed info about CSF.
